<?php
// get the data from the form
$coin_description = $_POST['coin'];
$us_dollars = $_POST['us_dollars'];

// calculate the crypto
switch ($_POST['coin']) {
    case 'btc':
        $btc_format = $us_dollars * 0.000095;
        $btc_formatted = number_format($btc_format, 6) . " Bitcoin";
        break;
    case 'bch':
        $bch_format = $us_dollars * 0.000664;
        $bch_formatted = number_format($bch_format, 6) . " Bitcoin Cash";
        break;
    case 'eth':
        $eth_format = $us_dollars * 0.001079;
        $eth_formatted = number_format($eth_format, 6) . " Etherium";
        break;
    case 'ltc':
        $ltc_format = $us_dollars * 0.004596;
        $ltc_formatted = number_format($ltc_format, 6) . " Litecoin";

    default:
        "Error";
}

// apply currency formatting to the dollar and percent amounts
$us_dollars_formatted = "$" . number_format($us_dollars, 2);

// escape the unformatted input
$coin_description_escaped = htmlspecialchars($coin_description);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Product Discount Calculator</title>
        <link rel="stylesheet" type="text/css" href="styles/main.css">
    </head>
    <body>
        <main>
            <img src="images/trade.png" alt="Coin" height="75" />
            <h1>Crypto Calculator</h1>

            <label>Coin Description:</label>
            <span><?php echo $coin_description_escaped; ?></span><br>

            <label>Dollars:</label>
            <span><?php echo $us_dollars_formatted; ?></span><br>

            <label>Coin:</label>
            <span><?php
                switch ($_POST['coin']) {
                    case 'btc':
                        echo $btc_formatted;
                        break;
                    case 'bch':
                        echo $bch_formatted;
                        break;
                    case 'eth':
                        echo $eth_formatted;
                        break;
                    case 'ltc':
                        echo $ltc_formatted;

                    default:
                        "Error";
                }
                ?></span><br
            <div id="buttons">
                <label>&nbsp;</label>
            </div>
        </main>
    </body>
</html>